#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <locale.h>
#include <unistd.h>
#include <termios.h>
#include <wchar.h>

#include "grille.h"

#define NEXT_CH_BUFFER_IS(ch) (!feof(stdin) && getc(stdin)==ch)

/*
* Sur Caséine le terminal fait 132 caractères de large
*
*/

typedef struct
{
        int lignes;
        int colonnes;
} ecran;


bool is_number(wchar_t string[])
{
        int i=0;
        if (!isdigit(string[0]) && string[0] != L'+' && string[0] != L'-')
                return false;
        i++;

        while (string[i] != L'\0')
        {
                if (!isdigit(string[i]))
                        return false;
                i++;
        }

        return true;
}

bool wcmp_fst_word(wchar_t str1[], wchar_t str2[])
{
        int i=0;
        while ((str1[i] != L' ' && str1[i] != L'\0') && (str2[i] != L' ' && str2[i] != L'\0'))
        {
                if (str1[i] != str2[i])
                        return false;
        }
        return true;
}

void flush_stdin()
{
        int c;
        while ((c=getchar()) != '\n' && c != EOF);
}

const char symbols[][5] = {
        "  ", // VIDE
        "\U0001f534", // red circle
        "\U0001f535", // blue circle
        "\U0001f7e2", // green circle
        "\U0001f7e1", // yellow circle
        "\U0001f7e0", // orange circle
        "\U0001f7e3", // purple circle
        "\U0001f7e4", // brown circle
        "\u26AB", // black circle
        "\u26AA", // white circle
};

void cur_goto(int line, int column)
{
        printf("\033[%d;%dH", line, column);
}

void clear_screen()
{
        printf("\033[2J");
}

void rect(int x1, int x2, int y1, int y2)
{
        cur_goto(y1, x1);
        printf("\u250C");
        cur_goto(y1, x2);
        printf("\u2510");
        cur_goto(y2, x1);
        printf("\u2514");
        cur_goto(y2, x2);
        printf("\u2518");

        for (int x=x1+1; x<x2; x++)
        {
                cur_goto(y1, x);
                printf("\u2500");
                cur_goto(y2, x);
                printf("\u2500");
        }

        for (int y=y1+1; y<y2; y++)
        {
                cur_goto(y, x1);
                printf("\u2502");
                cur_goto(y, x2);
                printf("\u2502");
        }
}

void print_rect_fichier(int x1, int x2, int y1, int y2, const char* nom_fichier, int offset_y)
{
        FILE * fichier = fopen(nom_fichier, "r");

        int x=x1, y=y1-offset_y;
        wchar_t ch = fgetwc(fichier);

        while (ch != EOF && y <= y2)
        {
                if (ch == '\n')
                {
                        x = x1;
                        y++;
                }
                else if (y >= y1)
                {
                        cur_goto(y, x);
                        printf("%lc", ch);
                        x++;
                }

                if (x>x2)
                {
                        y++;
                        x = x1;
                }
                ch = fgetwc(fichier);
        }
        
        fclose(fichier);
}

void print_rect(int x1, int x2, int y1, int y2, int* chaine, int offset_y)
{
        int x=x1, y=y1-offset_y;

        for (int *ch = chaine; *ch != '\0' && y <= y2; ch++)
        {
                if (x>x2)
                {
                        y++;
                        x = x1;
                }

                if (*ch == '\n')
                {
                        x = x1;
                        y++;
                }
                else if (y >= y1)
                {
                        cur_goto(y, x);
                        printf("%lc", *ch);
                        x++;
                }
        }
        
}

int **prompt_names(ecran ecr, const int prompt[], int n_joueurs)
{
        clear_screen();
        rect(ecr.colonnes/3, 2*ecr.colonnes/3, ecr.lignes/3, 2*ecr.lignes/3);

        getchar();
        return NULL;
}

int quot(int a, int b)
{
        return (a>=0) ? a/b : a/b-1;
}

void print_grille(ecran ecr, int base_x, int base_y, grille *g)
{
        clear_screen();
        if (ecr.lignes-base_y-1 < ecr.lignes && ecr.lignes > base_y+1)
        {
                for (int x=1; x<=ecr.colonnes; x+=2)
                {
                        cur_goto(ecr.lignes-base_y-2, x);
                        printf("\u2500\u2500"); // box drawing character
                        if ((quot((x+1),2)-base_x)%10==0 && x+2 < ecr.colonnes) printf("\v%d", quot(x+1,2)-base_x);
                }
        }

        for (int y=1; y<ecr.lignes-base_y-2 && y<ecr.lignes; y++)
        {
                for (int x=1; x<=ecr.colonnes; x+=2)
                {
                        cur_goto(y, x);
                        
                        jeton j = get_case(quot(x,2)-base_x, ecr.lignes-base_y-3-y, g);
                        printf("%s", symbols[j]);
                }
        }
        printf("\n");
}

void process_command(ecran ecr, int *offset_x, int *offset_y, bool *do_abort, bool *tour, grille *g)
{
        cur_goto(ecr.lignes, 0);
        printf(":");
        wchar_t c = getchar();
        wchar_t prompt[100] = L"";
        int i=0;

        while (c != '\n')
        {
                switch (c)
                {
                        case '\003':
                        case '\004':
                                *do_abort = true;
                                fprintf(stderr, (c == '\003') ? "^C\n" : "^D\n");
                                return;
                        case '\177':
                                if (i>0)
                                {
                                        prompt[i-1] = 0;
                                        i--;
                                }
                                break;
                        case '\033':
                                if (NEXT_CH_BUFFER_IS('['))
                                        switch (getchar())
                                        {
                                                case 'C': // ->
                                                        break;
                                                case 'D': // <-
                                                        if (i>0)
                                                                i--;
                                        }
                                else
                                        return;
                        default:
                                if (i<99)
                                {
                                        prompt[i] = c;
                                        prompt[i+1] = 0;
                                        i++;
                                }
                }
                cur_goto(ecr.lignes, 0);
                printf("\033[2K:%ls", prompt);
                cur_goto(ecr.lignes, i+2);
                c = getchar();
        }

        if (is_number(prompt))
        {
                *offset_x = ecr.colonnes/4 - wcstol(prompt, NULL, 10);
        }
        /* else if (wcmp_fst_word(prompt, L"save"))
        {
                printf("Saving...");
                getchar();
        } */
}

void process_input(ecran ecr, int *offset_x, int *offset_y, bool *do_abort, jeton joueur, int *last_x, int *last_y, grille *g)
{
        bool tour = true;
        while (tour && !*do_abort)
        {
                print_grille(ecr, *offset_x, *offset_y, g);
                cur_goto(0, ecr.colonnes/2+1);
                printf("%s", symbols[joueur]);

                switch (getchar())
                {
                        case ':':
                                process_command(ecr, offset_x, offset_y, do_abort, &tour, g);
                                break;
                        case '\003':
                                printf("^C\n");
                                *do_abort = true;
                                break;
                        case '\004':
                                printf("^D\n");
                                *do_abort = true;
                                break;
                        case '\033':
                                if (NEXT_CH_BUFFER_IS('['))
                                        switch(getc(stdin))
                                        {
                                                case 'A':
                                                        //printf("A"); break;
                                                        (*offset_y)--; break;
                                                case 'B':
                                                        //printf("B"); break;
                                                        (*offset_y)++; break;
                                                case 'C':
                                                        //printf("C"); break;
                                                        (*offset_x)--; break;
                                                case 'D':
                                                        //printf("D"); break;
                                                        (*offset_x)++; break;
                                        }
                                break;
                        case ' ':
                                *last_x = ecr.colonnes/4 - *offset_x;
                                ajouter_jeton(joueur, *last_x, last_y, g);
                                tour = false;
                                break;
                }
        }
}

bool read_cgu(ecran ecr)
{
        
        int offset_y=0;
        bool cgu_agree = false, cgu_disagree = false;
        while (!cgu_agree && !cgu_disagree)
        {
                clear_screen();
                rect(2, ecr.colonnes-1, 2, ecr.lignes-1);

                cur_goto(3,4);
                print_rect_fichier(4, ecr.colonnes-3, 3, ecr.lignes-2, "CGU.txt", offset_y);
                
                switch (getchar())
                {
                        case '\n':
                                cgu_agree = true; break;
                        case '\033': // ESC
                                if (NEXT_CH_BUFFER_IS('['))
                                        switch(getchar())
                                        {
                                                case 'A': if (offset_y>0) offset_y--; break;
                                                case 'B': offset_y++; break;
                                        }
                                break;
                        case '\003':
                        case '\004':
                                cgu_disagree = true;
                                break;
                }
        }

        return cgu_agree;
}

void jouer(ecran ecr, int n_joueurs)
{
        grille *g = creer_grille();

        int offset_x = ecr.colonnes/4, offset_y = 0;
        int last_x = 0, last_y = 0;
        bool do_abort = false;

        jeton joueur = 0;

        do {
                joueur = joueur%n_joueurs + 1;

                process_input(ecr, &offset_x, &offset_y, &do_abort, joueur, &last_x, &last_y, g);
        } while (!gagnant(last_x, last_y, 5, g) && !do_abort);

        if (!do_abort)
        {
                print_grille(ecr, offset_x, offset_y, g);
                cur_goto(0,0);
                printf("Le joueur %s a gagné. Appuyer sur une touche pour continuer...", symbols[joueur]);
                getchar();
                cur_goto(0,0);
                clear_screen();
        }

        detruire_grille(g);
}

int main()
{
        setlocale (LC_ALL, "");

        static struct termios oldterm, newterm;

        tcgetattr(STDIN_FILENO, &oldterm);
        newterm = oldterm;

        newterm.c_lflag &= ~(ICANON | IEXTEN | ISIG | ECHO | ECHONL);
        tcsetattr(STDIN_FILENO, TCSANOW, &newterm);

        ecran ecr = {20, 80};

        setvbuf(stdin, NULL, _IONBF, 4);

        printf("\033[?1049h"); // écran alternatif

        if (read_cgu(ecr))
        {
                //prompt_names(ecr, L"Joueurs ?", 2);
                jouer(ecr, 2);
        }
        

        printf("\033[?1049l"); // écran initial
        tcsetattr(STDIN_FILENO, TCSANOW, &oldterm);

        flush_stdin();

        return 0;
}
