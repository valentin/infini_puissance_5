#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>

#define ATOM_SIZE 3

typedef wchar_t pixel;

typedef struct {
        int width, height;
        pixel **pixels;
} ecran;

ecran* creer_ecran(int width, int height)
{
        ecran* ecr = malloc(sizeof(ecran));

        ecr->width = width;
        ecr->height = height;
        ecr->pixels = malloc(height * sizeof(pixel));

        for (int i=0; i<ecr->height; i++)
                ecr->pixels[i] = malloc((width+1) * sizeof(pixel));

        return ecr;
}

void detruire_ecran(ecran* ecr)
{
        for (int i=0; i<ecr->height; i++)
                free(ecr->pixels[i]);
        
        free(ecr->pixels);
        free(ecr);
}

void set_px(pixel px, int x, int y, ecran *ecr)
{
        ecr->pixels[y][x] = px;
}

pixel get_px(int x, int y, ecran *ecr)
{
        return ecr->pixels[y][x];
}

void print_ecran(ecran *ecr)
{
        for (int i=0; i<ecr->height; i++)
                printf("%ls\n", ecr->pixels[i]);
}


int main(void)
{
        setlocale(LC_ALL, "");

        ecran *ecr = creer_ecran(100, 50);

        print_ecran(ecr);

        detruire_ecran(ecr);
}
