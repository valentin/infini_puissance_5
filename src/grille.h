#ifndef GRILLE_H_INCLUDED
#define GRILLE_H_INCLUDED

#include <stdio.h>
#include <stdbool.h>

#define X_BLOCK_SIZE 10
#define Y_BLOCK_SIZE 10

typedef int jeton;

typedef struct colonne {
	int hauteur;
	int capacite;
	jeton* jetons;
} colonne;

typedef struct grille {
	int n_positifs;
	colonne **positifs;
	int n_negatifs;
	colonne **negatifs;
} grille;


grille *creer_grille();

colonne *get_colonne(int i, grille *g);

jeton get_case(int x, int y, grille *g);

bool ajouter_jeton(jeton j, int x, int *y, grille *g);

void detruire_grille(grille *g);

bool gagnant(int x, int y, int N, grille *g);

jeton get_case(int x, int y, grille *g);

void sauvegarder(FILE *stream, grille *g);

grille *charger(FILE *stream);

#endif /* GRILLE_H_INCLUDED */
