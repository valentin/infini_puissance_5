/*
 * Infini puissance N : un jeu à plusieurs joueurs.
 *  Copyright (C) 2023  Valentin Moguérou
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h> // pour la gestion des fichiers
#include <stdlib.h>
#include <stdbool.h>

#include "grille.h"

/*
void print_tab(jeton *tab, int n)
{
        for (int i=0; i<n; i++)
                printf("%p --> %x (%d)\n", (void*)&tab[i], tab[i], tab[i]);
        printf("\n");
}

void print_colonne(colonne *col)
{
        for (int i=0; i<col->capacite; i++)
                putc(repr_jeton(col->jetons[i]), stdout);
        putc('\n', stdout);
}
*/

void init_zeros(jeton* ptr, int count)
{
        for (int i=0; i<count; i++)
        {
                ptr[i] = 0;
        }
}

colonne *creer_colonne(int capacite)
{
        colonne *col = malloc(sizeof(colonne));
        if (col == NULL)
                return NULL;

        col->jetons = malloc(capacite*sizeof(jeton));
        if (col->jetons == NULL)
        {
                free(col);
                return NULL;
        }

        init_zeros(col->jetons, capacite);

        col->capacite = capacite;
        col->hauteur = 0;

        return col;
}

bool agrandir_colonne(int diff_taille, colonne *col)
{
        jeton *jetons_nouv = realloc(col->jetons, (col->capacite + diff_taille)*sizeof(jeton));
        if (jetons_nouv == NULL)
                return false; // allocation impossible, on garde col->jetons tel quel
        
        col->jetons = jetons_nouv;

        // on met des zéros dans la partie nouvellement attribuée
        init_zeros(col->jetons + col->capacite, diff_taille);

        // free est appelée par realloc et les éléments sont copiés par realloc
        col->capacite += diff_taille;

        return true;

}

bool ajouter_jeton_col(jeton j, colonne *col, int* y)
{
        if (col->hauteur >= col->capacite)
                if (!agrandir_colonne(Y_BLOCK_SIZE, col))
                        return false;

        col->jetons[col->hauteur] = j;
        col->hauteur++;
        
        if (y != NULL) *y = col->hauteur - 1;

        return true;
}

jeton get_jeton_col(int indice, colonne *col)
{
        return (indice < col->hauteur && indice >= 0) ? col->jetons[indice] : 0;
}

void detruire_colonne(colonne *col)
{
        if (col == NULL) return;

        free(col->jetons);
        free(col);
}

/*
 * On essaie de représenter une structure abstraite comme ceci :
 * En abscisse, les "colonne*"; en ordonnée, les "jeton"                                
 *
 *                               +oo (jetons)
 *                                ^
 *                                |
 *                                |
 *                              |
 *                                |
 *                              |
 * -oo <------------------------I----------------------> +oo (colonne*)
 *
 *  Représentation en mémoire :
 *
 *  g->positifs = [colonne*(0), colonne*(1), ...]
 *                  // autant que de colonnes positives ou nulles ç.à.d autant que g->n_positifs
 *  
 *  g->negatifs = [colonne*(-1), colonne*(-2) , ...]
 *                  // autant que de colonnes str. négatives ç.à.d autant que g->n_negatifs
 */ 

grille *creer_grille()
{
        /* On divise largeur en deux :
         *        
         *        |-----------------------------|-----------------------------|
         *        negatifs                      0                             positifs
         *
         *        d'où n_positifs = largeur/2 + largeur%2
         *             n_negatifs = largeur/2
         *        
         *        de telle sorte que:
         *                n_negatifs + n_positifs = largeur
         *
         *         Ex :
         *         -        L'intervalle [-10, 9] de cardinal 20 se découpe en
         *                 10 nombres positifs [0, 9] et 10 nombres négatifs [-10, -1] représenté
         */

        grille *g = malloc(sizeof(grille));
        if (g == NULL)
                return NULL;

        g->n_positifs = X_BLOCK_SIZE/2 + X_BLOCK_SIZE%2;
        g->n_negatifs = X_BLOCK_SIZE/2;
        g->positifs = malloc(g->n_positifs * sizeof(colonne*));
        g->negatifs = malloc(g->n_negatifs * sizeof(colonne*));
        
        if (g->positifs == NULL || g->negatifs == NULL)
        {
                free(g->positifs);
                free(g->negatifs);
                free(g);
                return NULL;
        }

        bool echec_allocation = false;

        for (int i=0; i < g->n_positifs; i++)
        {
                g->positifs[i] = creer_colonne(Y_BLOCK_SIZE);
                if (g->positifs[i] == NULL)
                        echec_allocation = true;
        }
        
        for (int i=0; i < g->n_negatifs; i++)
        {
                g->negatifs[i] = creer_colonne(Y_BLOCK_SIZE);
                if (g->negatifs[i] == NULL)
                        echec_allocation = true;
        }


        // si une colonne n'a pas pu être crée, on détruit la grille
        if (echec_allocation)
        {
                detruire_grille(g);
                return NULL;
        } 
        
        return g;
}


bool etendre_tab(int d_len, int *len, colonne ***tab)
{
        /* Fonction qui prend en entrée une différence de taille, un pointeur
         * vers un tableau de colonnes et un pointeur vers sa longueur.
         * 
         * Si la réallocation n'échoue pas (99.99% des cas), le pointeur vers
         * le tableau est éventuellement modifié (selon la tambouille de realloc)
         * et la taille est modifiée.
         *
         * Un argument est un colonne*** car c'est un pointeur vers un tableau de colonne*
         *
         */

        colonne **tab_nouv = realloc(*tab, (*len + d_len)*sizeof(colonne*));
        if (tab_nouv == NULL)
                return false; // la mémoire n'a pas pu être allouée
        
        *tab = tab_nouv;

        bool echec_allocation = false;
        for (int i=0; i<d_len; i++)
        {
                (*tab)[*len + i] = creer_colonne(Y_BLOCK_SIZE);
                if ((*tab)[*len + i] == NULL)
                        echec_allocation = true;
        }

        // si échec on revient à l'état initial
        if (echec_allocation)
        {
                for (int i=0; i<d_len; i++)
                        if ((*tab)[*len + i] != NULL)
                                detruire_colonne((*tab)[*len + i]);
                return false;
        }

        *len += d_len;

        return true;
}


bool etendre_gauche(int d_len, grille *g)
{
        // application sur les négatifs
        return etendre_tab(d_len, &g->n_negatifs, &g->negatifs);
}

bool etendre_droite(int d_len, grille *g)
{
        // application sur les positifs
        return etendre_tab(d_len, &g->n_positifs, &g->positifs);
}


colonne *get_colonne(int i, grille *g)
{
        /* La structure de tableau double sens se traduit de la façon suivante :
         *
         * Si i >= 0, on regarde g->positifs[i]
         * Si i < 0, on regarde g->negatifs[-i-1]
         */

        if (i >= 0 && i < g->n_positifs)
                return g->positifs[i];
        else if (i < 0 && ~i < g->n_negatifs)
                return g->negatifs[~i];
        else
                return NULL; // en dehors de l'allocation
}

jeton get_case(int x, int y, grille *g)
{
        colonne *col = get_colonne(x, g);
        return (col != NULL) ? get_jeton_col(y, col) : 0;
}

int ceil_div(int a, int b)
{
        return (a%b == 0) ? (a/b) : (a/b + 1);
}

int next_step(int x, int step)
{
        return step * ceil_div(x, step);
}


bool ajouter_jeton(jeton j, int x, int *y, grille *g)
{
        if (x >= 0 && x >= g->n_positifs)
        {
                if (!(etendre_droite(next_step(x - g->n_positifs + 1, X_BLOCK_SIZE), g)))
                        return false;
        }

        if (x < 0 && ~x >= g->n_negatifs)
        {
                if (!(etendre_gauche(next_step(~x - g->n_negatifs + 1, X_BLOCK_SIZE), g)))
                        return false;
        }

        return ajouter_jeton_col(j, get_colonne(x, g), y);
}


void detruire_grille(grille *g)
{
        for (int i=0; i < g->n_positifs; i++)
                detruire_colonne(g->positifs[i]);

        for (int i=0; i < g->n_negatifs; i++)
                detruire_colonne(g->negatifs[i]);

        free(g);
}


bool gagnant_aux(int x, int y, int dx, int dy, int n, grille *g)
{
        if (n==1)
                return true;

        return (get_case(x+dx, y+dy, g) == get_case(x, y, g) && get_case(x, y, g) != 0)
                ? gagnant_aux(x+dx, y+dy, dx, dy, n-1, g)
                : false;
}        

bool gagnant(int x, int y, int N, grille *g)
{
        /* Fonction qui renvoie si le dernier joueur gagne
         * en fonction de la position du dernier joueur.
         *
         * x: abscisse du dernier jeton posé
         * y: ordonnée du dernier jeton posé
         * g: grille de travail
         */

        for (int dx = -1; dx < 2; dx++)
                for (int dy = -1; dy < 2; dy++)
                        if ((dx != 0 || dy != 0) && gagnant_aux(x, y, dx, dy, N, g))
                                return true;
        
        return false;
}

void sauvegarder_colonne(FILE *stream, colonne *c)
{
        for (int i=0; i<c->hauteur; i++)
                if (c->jetons[i] != 0)
                        fprintf(stream, "%d\n", c->jetons[i]);
}

void sauvegarder(FILE *stream, grille *g)
{
        for (int i=0; i<g->n_positifs; i++)
        {
                fprintf(stream, "%d;", i);
                sauvegarder_colonne(stream, g->positifs[i]);
        }

        for (int i=0; i<g->n_negatifs; i++)
        {
                fprintf(stream, "%d;", ~i);
                sauvegarder_colonne(stream, g->negatifs[i]);
        }
}

grille *charger(FILE *stream)
{
        grille *g = creer_grille();

        int col, joueur;
        while (fscanf(stream, "%d;%d", &col, &joueur) == 3)
                ajouter_jeton(joueur, col, NULL, g);

        return g;
}

// =========================================== TEST =================================================

/*


int test_colonne()
{
        colonne *col = creer_colonne(Y_BLOCK_SIZE);
        print_colonne(col);

        int y;
        for (int i=0; i<241; i++)
        {
                if (ajouter_jeton_col((i%2==0) ? ROUGE : BLEU, col, &y))
                        printf("Jeton ajouté à hauteur %d.\n", y);
                else
                        fprintf(stderr, "Erreur dans l'ajout d'un jeton.\n");
        }

        print_colonne(col);

        detruire_colonne(col);

        return 0;
}

void print_grille_temp(grille *g)
{
        printf("n_positifs : %d ; n_negatifs : %d\n", g->n_positifs, g->n_negatifs);
        for (int i=-g->n_negatifs; i<g->n_positifs; i++)
        {
                printf("% 5d | ", i);
                print_colonne(get_colonne(i, g));
        }

}

int test_grille()
{
        grille *g = creer_grille();

        int y;
        for (int i=0; i<6; i++)
        {
                ajouter_jeton(BLEU, 0, &y, g);
               
                print_grille_temp(g);
                printf("\n\n\n\n\n");

                if (gagnant(0, y, 5, g))
                        printf("Le dernier joueur a gagné.\n");
        }



        detruire_grille(g);

        return 0;
}


jeton jouer()
{
        grille *g = creer_grille();
        int tour = 0;

        int x, y;
        do {
                print_grille_temp(g);

                printf("Test %c\n", repr_jeton(get_case(-50, -50, g)));

                printf("Tour %d, joueur %c. Entrer colonne : ", tour, repr_jeton((jeton)(tour%2 + 1)));
                scanf("%d", &x);

                ajouter_jeton((jeton)(tour%2 + 1), x, &y, g);

                tour++;
        } while (!gagnant(x, y, 5, g));

        print_grille_temp(g);
        
        jeton gagnant = (tour-1)%2 + 1;
        printf("Le gagnant est %c.\n", repr_jeton(gagnant));

        return gagnant;
}


int main(void)
{
        jouer();
        return 0;
}
*/
