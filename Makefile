CC=gcc
CFLAGS=-Wall -Wextra -Wpedantic
LDFLAGS=

bin/cli: obj/grille.o obj/cli.o
	@mkdir -p bin
	$(CC) -o $@ $^ $(LDFLAGS)

obj/grille.o: src/grille.c src/grille.h
	@mkdir -p obj
	$(CC) -o $@ -c $< $(CFLAGS)

obj/cli.o: src/cli.c
	@mkdir -p obj
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean

clean:
	rm -rf bin/ obj/
